#!/usr/bin/env sh

[ "$#" -eq 1 ] && gawk '/^#!.*( |[/])sh/{printf "%s\0", FILENAME} {nextfile}' "$(readlink -f "$1")/*" | xargs -0 checkbashisms
