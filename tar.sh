#!/usr/bin/env sh

if [ $# -eq 1 ]; then
    name="$1"
else
    name="$(basename "$(pwd)")"
fi

files="$@"
tar -cvzf $name.tar.gz $files
