#!/usr/bin/env sh

[ -z "$1" ] && exit 1

columns="$(printf "%b\n" "${1}" | awk -F ":" '{print NF-1}')"

first="$(printf "%b\n" "$1" | cut -d ":" -f 1)"
second="$(printf "%b\n" "$1" | cut -d ":" -f 2)"
third="$(printf "%b\n" "$1" | cut -d ":" -f 3)"

case $columns in
    1)  seconds=$(( $first*60 + $second ))
        ;;
    2)  seconds=$(( $first*60*60 + $second*60 + $third ))
        ;;
    *) exit 1
        ;;
esac

printf "%b\n" "$seconds"
