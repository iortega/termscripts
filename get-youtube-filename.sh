#!/usr/bin/env sh

youtube-dl --merge-output-format mkv --get-filename "$1"
