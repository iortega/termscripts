#!/usr/bin/env sh

iter="1"
while [ $iter -eq "1" ];
do
    freq=$(top -bn 2 -d 5 | grep '^%Cpu' | tail -n 1 | gawk '{print $2+$4+$6}')
    iter=$(echo "$freq > 5" | bc -l)
    if [ $iter -eq 0 ]; then
        sleep 5m
        freq=$(top -bn 2 -d 5 | grep '^%Cpu' | tail -n 1 | gawk '{print $2+$4+$6}')
        iter=$(echo "$freq > 5" | bc -l)
    else
        sleep 2m
    fi
done

printf "%b\n" "done"
sleep 10m
