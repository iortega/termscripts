#!/usr/bin/env sh

ret="$(top -bn 2 -d 5 | grep '^%Cpu' | tail -n 1 | gawk '{print $2+$4+$6}')"

printf "%b\n" "$ret" > /tmp/cpu-freq
