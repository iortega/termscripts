#!/usr/bin/env sh

folder=$(dirname "$SCRIPTS")
file="$(find -L "$SCRIPTS" "$TSCRIPTS" -regex ".*\.\(ba\)?sh\$" \
	-type f | sed "s|^$folder/||" | fzf -i)"
[ -z "$file" ] || $EDITOR "$folder/$file"
