#!/usr/bin/env sh

# [ -z "${TERMINAL##*termite*}" ] && $TERMINAL -e "$EDITOR \"$file\"" \
#             || $TERMINAL -e $EDITOR "$file"
[ -z $TERMINAL ] && exit 1

pars="dash -c \"$@\""
case $TERMINAL in
    *termite*) $TERMINAL -e "$pars" ;;
    *st*) dash -c "$TERMINAL -e $pars" ;;
    *) dash -c "$TERMINAL -e $pars" ;;
esac
