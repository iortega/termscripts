#!/usr/bin/env sh

[ -z "$1" ] && exit 1

# Get the filename
filename="$(dash $TSCRIPTS/get-youtube-filename.sh "$1")"

if [ ! -f "$filename" ]; then
    # Download the file
    # printf "%b\n" "youtube-dl --merge-output-format mkv $1"
    youtube-dl --merge-output-format mkv "$1"
fi


printf "%b\n" "$filename"
