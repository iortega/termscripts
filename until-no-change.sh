#!/usr/bin/env sh

current=$(mktemp '/tmp/until-no-change-currentXXXXXXX')
prev=$(mktemp '/tmp/until-no-change-prevXXXXXXX')
sh -c "$1" > "$current"

while [ -n "$(diff "$current" "$prev")" ]; do
    cp "$current" "$prev"
    sh -c "cat '$prev' | $2 > '$current'"
done

cat "${current}"
