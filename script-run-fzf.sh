#!/usr/bin/env sh

folder=$(dirname "$SCRIPTS")
files=$(find -L "$SCRIPTS/" "$TSCRIPTS/" -regex ".*\.\(ba\)?sh\$" -type f |
	sed "s|^$folder/||")

# printf "%b\n" "${files}"

file="$(printf "%b\n" "${files}" | fzf -i)"
# printf "%b\n" "${file}"

[ -n "$file" ] && {
    if echo "$file" | grep -q "\.bash\$"; then
        bash "$folder/$file"
    else
        dash "$folder/$file"
    fi
}
